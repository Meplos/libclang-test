#pragma once

#include "Graphic.h"

class Text : public Graphic
{
private:
    /* data */
public:
    Text(/* args */);
    ~Text();
    virtual void draw();
    virtual void add(Graphic &g);
    virtual void remove(Graphic &g);
    virtual void text();
};

