#pragma once

#include "Graphic.h"

class Rectangle : public Graphic
{
private:
    /* data */
public:
    Rectangle(/* args */);
    ~Rectangle();
    virtual void draw();
    virtual void add(Graphic &g);
    virtual void remove(Graphic &g);
    virtual void text();
};

