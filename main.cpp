#include <iostream>
#include <clang-c/Index.h>
#include <clang-c/CXCompilationDatabase.h>

#include <list>
#include "ListOfClasse.h"
using namespace std;

ostream& operator<<(ostream& stream, const CXString& str)
{
  stream << clang_getCString(str);
  clang_disposeString(str);
  return stream;
}


int main()
{

  ListOfClasse listofclass;
  const char* const tab[2] = {"-x","c++"}; 
 
  CXIndex index = clang_createIndex(1,0);
  CXTranslationUnit unit = clang_parseTranslationUnit(index, "./A-B_association/main.cpp",tab,2,nullptr,0,CXTranslationUnit_None);
  if(unit == nullptr) {
    cerr << "Unable to parse translation unit. Quitting." << endl;
    exit(-1);
  }   
  CXCursor cursor = clang_getTranslationUnitCursor(unit);
  
  clang_visitChildren(
    cursor,
    [](CXCursor c, CXCursor parent, CXClientData client_data){
      cout << "Cursor '" << clang_getCursorSpelling(c) << "' of kind '"
        << clang_getCursorKindSpelling(clang_getCursorKind(c)) <<"'\n";
      if(clang_getCursorKind(c) == CXCursor_ClassDecl){
        CXString cx_class_name = clang_getCursorSpelling(c);
        string class_name = clang_getCString(cx_class_name);
        ((ListOfClasse*) client_data)->add(class_name);
        clang_disposeString(cx_class_name);
      }
      if(clang_getCursorKind(c) == CXCursor_CXXBaseSpecifier){
        cout << "INHERITANCE" << endl;
      }
      return CXChildVisit_Recurse;
  },
  &listofclass);
  clang_disposeTranslationUnit(unit);
  clang_disposeIndex(index);


  list<string> liste = listofclass.getList();
  list<string>::iterator it;


  for(it = liste.begin(); it != liste.end(); ++it) {
    cout << *it << endl;
  }
  

}