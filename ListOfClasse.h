#pragma once
#include <list>
#include <iostream>

using namespace std;

class ListOfClasse
{
private:
    list<string> classes;
public:
    ListOfClasse(/* args */);
    ~ListOfClasse();
    void add(string);
    list<string> getList();
    bool isIn(string);
};


