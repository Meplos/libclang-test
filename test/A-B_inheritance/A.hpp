#pragma once

#include "B.hpp"

class A : public B
{
    private:
        /* data */
    public:
        A(/* args */);
        ~A();
};
