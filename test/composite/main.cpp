#include "Graphic.h"
#include "Line.h"
#include "Rectangle.h"
#include "Text.h"
#include "Picture.h"

int main(int argc, char const *argv[])
{
    Graphic *g = new Picture();

    g->add(new Line());
    g->add(new Rectangle());
    g->add(new Text());
    

    return 0;
}
