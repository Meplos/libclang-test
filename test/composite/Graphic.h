#pragma once

class Graphic
{
private:

public:
    virtual void draw() = 0;
    virtual void add(Graphic*) = 0;
    virtual void remove(Graphic*) = 0;
    virtual void text() = 0;

};
