#pragma once

#include "Graphic.h"
#include "list"
class Picture : public Graphic
{
private:
    std::list<Graphic> graphic;
public:
    Picture(/* args */);
    ~Picture();
    virtual void draw();
    virtual void add(Graphic* g);
    virtual void remove(Graphic* g);
    virtual void text();
    Graphic * getChild(int);
};

