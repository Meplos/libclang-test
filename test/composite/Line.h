#pragma once
#include "Graphic.h"

class Line : public Graphic
{
private:
    /* data */
public:
    Line(/* args */);
    ~Line();
    virtual void draw();
    virtual void add(Graphic &g);
    virtual void remove(Graphic &g);
    virtual void text();
};



