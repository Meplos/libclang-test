#ifndef B_CLASS 
#define B_CLASS

#include "./A.hpp"

class B
{
    private:
        A a();
    public:
        B();
        ~B();
};

#endif
