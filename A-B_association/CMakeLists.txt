cmake_minimum_required(VERSION 3.0)

project(a-b_association)

set(CMAKE_CXX_COMPILER
	g++
)

set(CMAKE_CC_COMPILER
	gcc
)
set(SRCS
	main.cpp
	A.cpp
	B.cpp
)

set(HEADERS
	A.h
	B.h
)

add_executable(main ${SRCS})
